pub mod ffi;
mod primitives;
mod structures;

pub use primitives::*;
pub use structures::*;

#[cfg(test)]
mod tests {
    use crate::World;
    #[test]
    fn it_works() {
        let mut world = World::new();
        world
            .set_gravity(9.0, 2.0, 2.0)
            .set_erp(0.8)
            .set_cfm(0.0000000001)
            .set_auto_disable_flag(true);
        assert_eq!(world.get_gravity()[0..3], [9.0, 2.0, 2.0]);
        assert_eq!(world.get_erp(), 0.8);
        assert_eq!(world.get_cfm(), 0.0000000001);
        assert!(world.get_auto_disable_flag());
        let mut body = world.create_body();
        body.set_position(2.0, 1.0, 3.0)
            .set_quaternion([0.0, 1.0, 0.0, 0.0]);
        assert_eq!(body.get_position()[0..3], [2.0, 1.0, 3.0]);
        assert_eq!(body.get_quaternion(), [0.0, 1.0, 0.0, 0.0]);
        assert_eq!(
            body.get_rotation(),
            [1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0]
        );
    }
}
