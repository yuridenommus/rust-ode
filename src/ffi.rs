#![allow(clippy::all)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
include!(concat!(env!("OUT_DIR"), "/ffi.rs"));

#[cfg(test)]
mod tests {
    use crate::ffi::{dODE_VERSION, dWorldCreate, dWorldDestroy};
    #[test]
    fn ode_version() {
        unsafe {
            let world = dWorldCreate();
            dWorldDestroy(world);
        }
        assert_eq!(dODE_VERSION, b"0.16.0\0");
    }
}
