use crate::ffi;
use crate::primitives::{dMatrix3, dQuaternion, dReal, dVector3};

pub struct World {
    world: ffi::dWorldID,
}

impl World {
    pub fn new() -> World {
        World {
            world: unsafe { ffi::dWorldCreate() },
        }
    }

    pub fn set_gravity(&mut self, x: dReal, y: dReal, z: dReal) -> &mut Self {
        unsafe {
            ffi::dWorldSetGravity(self.world, x, y, z);
        }
        self
    }

    pub fn get_gravity(&self) -> dVector3 {
        unsafe {
            let mut gravity: dVector3 = std::mem::uninitialized();
            ffi::dWorldGetGravity(self.world, (&mut gravity).as_mut_ptr());
            gravity
        }
    }

    pub fn set_erp(&mut self, erp: dReal) -> &mut Self {
        unsafe {
            ffi::dWorldSetERP(self.world, erp);
        }
        self
    }

    pub fn get_erp(&self) -> dReal {
        unsafe { ffi::dWorldGetERP(self.world) }
    }

    pub fn set_cfm(&mut self, cfm: dReal) -> &mut Self {
        unsafe {
            ffi::dWorldSetCFM(self.world, cfm);
        }
        self
    }

    pub fn get_cfm(&self) -> dReal {
        unsafe { ffi::dWorldGetCFM(self.world) }
    }

    pub fn set_auto_disable_flag(&mut self, do_auto_disable: bool) -> &mut Self {
        unsafe {
            ffi::dWorldSetAutoDisableFlag(self.world, do_auto_disable as std::os::raw::c_int);
        }
        self
    }

    pub fn get_auto_disable_flag(&self) -> bool {
        unsafe { ffi::dWorldGetAutoDisableFlag(self.world) != 0 }
    }

    pub fn step(&mut self, stepsize: dReal) {
        unsafe {
            ffi::dWorldStep(self.world, stepsize);
        }
    }

    pub fn quick_step(&mut self, stepsize: dReal) {
        unsafe {
            ffi::dWorldQuickStep(self.world, stepsize);
        }
    }

    pub fn create_body(&self) -> Body {
        Body::new(self)
    }
}

impl Default for World {
    fn default() -> Self {
        Self::new()
    }
}

impl Drop for World {
    fn drop(&mut self) {
        unsafe {
            ffi::dWorldDestroy(self.world);
        }
    }
}

pub struct Body<'a> {
    body: ffi::dBodyID,
    _world: std::marker::PhantomData<&'a World>,
}

impl<'a> Body<'a> {
    pub fn new(world: &'a World) -> Body<'a> {
        Body {
            body: unsafe { ffi::dBodyCreate(world.world) },
            _world: std::marker::PhantomData,
        }
    }

    pub fn set_position(&mut self, x: dReal, y: dReal, z: dReal) -> &mut Self {
        unsafe {
            ffi::dBodySetPosition(self.body, x, y, z);
        }
        self
    }

    pub fn get_position(&self) -> dVector3 {
        unsafe {
            let mut array: dVector3 = std::mem::uninitialized();
            let slice = std::slice::from_raw_parts(ffi::dBodyGetPosition(self.body), 4);
            array.copy_from_slice(slice);
            array
        }
    }

    pub fn set_rotation(&mut self, mut rotation: dMatrix3) -> &mut Self {
        unsafe {
            ffi::dBodySetRotation(self.body, (&mut rotation).as_mut_ptr());
        }
        self
    }

    pub fn get_rotation(&self) -> dMatrix3 {
        unsafe {
            let mut array: dMatrix3 = std::mem::uninitialized();
            let slice = std::slice::from_raw_parts(ffi::dBodyGetRotation(self.body), 12);
            array.copy_from_slice(slice);
            array
        }
    }

    pub fn set_quaternion(&mut self, mut quaternion: dQuaternion) -> &mut Self {
        unsafe {
            ffi::dBodySetQuaternion(self.body, (&mut quaternion).as_mut_ptr());
        }
        self
    }

    pub fn get_quaternion(&self) -> dQuaternion {
        unsafe {
            let mut array: dQuaternion = std::mem::uninitialized();
            let slice = std::slice::from_raw_parts(ffi::dBodyGetQuaternion(self.body), 4);
            array.copy_from_slice(slice);
            array
        }
    }
}

impl<'a> Drop for Body<'a> {
    fn drop(&mut self) {
        unsafe {
            ffi::dBodyDestroy(self.body);
        }
    }
}
