extern crate bindgen;
extern crate cmake;
extern crate filetime;

use filetime::FileTime;
use std::env;
use std::fs;

fn generate_bindings(out_dir: &str) {
    let ode_dir = format!("{}/build", out_dir);
    let bindings = bindgen::Builder::default()
        .header("ode/include/ode/ode.h")
        .clang_arg("-I./ode/include")
        .clang_arg(format!("-I{}/include", ode_dir))
        .blacklist_type("_bindgen_ty_1")
        .blacklist_type("max_align_t")
        .generate()
        .expect("Unable to generate bindings");

    bindings
        .write_to_file(format!("{}/ffi.rs", out_dir))
        .expect("Couldn't write file");
}

fn compile_cmake() {
    cmake::Config::new("ode").build_target("").build();
}

fn exec_if_newer<F: Fn()>(inpath: &str, outpath: &str, build: F) {
    if let Ok(metadata) = fs::metadata(outpath) {
        let outtime = FileTime::from_last_modification_time(&metadata);
        let intime = FileTime::from_last_modification_time(
            &fs::metadata(inpath).expect(&format!("Path {} not found", inpath)),
        );
        if outtime > intime {
            return;
        }
    }
    build();
}

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();

    exec_if_newer("ode", &format!("{}/build", out_dir), compile_cmake);

    exec_if_newer("ode", &format!("{}/ffi.rs", out_dir), &|| {
        generate_bindings(&out_dir)
    });

    println!("cargo:rustc-link-search={}/build", out_dir);
    println!("cargo:rustc-link-lib=ode");
}
